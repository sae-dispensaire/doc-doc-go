
package saedispensaire;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author hugom
 */
public class NegativeDistanceException extends Exception{
    public NegativeDistanceException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Distance must not be negative","Error",JOptionPane.WARNING_MESSAGE);
    }
}
