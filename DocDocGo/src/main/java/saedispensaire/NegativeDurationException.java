
package saedispensaire;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author hugom
 */
public class NegativeDurationException extends Exception{
    public NegativeDurationException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Duration must not be negative","Error",JOptionPane.WARNING_MESSAGE);
    }
}
