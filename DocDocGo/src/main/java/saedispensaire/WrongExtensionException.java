
package saedispensaire;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class WrongExtensionException extends Exception{
    public WrongExtensionException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Wrong file extension format","Error",JOptionPane.WARNING_MESSAGE);
    }
}
