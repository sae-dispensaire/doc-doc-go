
package saedispensaire;

import java.util.LinkedList;


public class Chemin {
   public LinkedList<Sommet> meilleurchemin;
   public double fiabilite;
   public double distance;
   public double duree;
   
   public Chemin(){
       meilleurchemin=new LinkedList<>();
       fiabilite=1;
       distance=Double.POSITIVE_INFINITY;
       duree=Double.POSITIVE_INFINITY;
   }
   
   public String toString(){
       return meilleurchemin.toString()+"| fiabilite : "+fiabilite+"| distance : "+distance+"| duree : "+duree;
   }
}
