
package saedispensaire;

/**
 *
 * @author hugom , ricardo
 */
public class Arete {

    private int idarete;
    private static int nbaretecree=0;
    private double fiabilite;
    private double distance;
    private double duree;
    private Sommet[] extremite = new Sommet[2];

    public Arete(double fiable, double distances, double dure, Sommet Sommet1, Sommet Sommet2){
        idarete = ++nbaretecree;
        fiabilite = fiable;
        distance = distances;
        duree = dure;
        extremite[0] = Sommet1;
        extremite[1] = Sommet2;
    }

    protected static void resetnbarete(){
        nbaretecree=0;
    }
    
    public String toString(){
        return (extremite[0]+"-"+extremite[1]);
    }

    public int getidarete(){
        return idarete;
    }
    
    public double getdist(){
        return distance;
    }
    
    public double getfiable(){
        return fiabilite;
    }
    
    public double getduree(){
        return duree;
    }
    
    public Sommet getExtremite(int n){
        return extremite[n];
    }

    public void setDist(double dist){
        distance = dist;
    }

    public void setFiable(double fiable){
        fiabilite = fiable;
    }
    
    public void setDuree(double dure){
        duree = dure;
    }

    public static int getnbarete(){
        return nbaretecree;
    }
}
