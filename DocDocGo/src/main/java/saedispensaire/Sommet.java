package saedispensaire;

import java.util.*;
import saedispensaire.ListArete.MaillonLA;

public class Sommet {

    private int idsommet;
    private static int nbsommetcree=1;
    private String nomsommet;
    private ListArete listareteadjacente;
    private int typesommet;

    public Sommet(String nom,int type){
        idsommet=nbsommetcree++;
        nomsommet=nom;
        typesommet=type;
        listareteadjacente=new ListArete();
    }

    protected static void resetnbsommet(){
        nbsommetcree=1;
    }
    
    @Override
    public String toString(){
        return(nomsommet);
    }

    public Arete addArete(double fiable, double distances, double dure, Sommet Sommetdest){
        Arete newarete= new Arete(fiable, distances, dure, this, Sommetdest);
        listareteadjacente.addMaillonLA(newarete);
        Sommetdest.addArete(newarete);
        return newarete;
    }

    public void addArete(Arete arete){
        listareteadjacente.addMaillonLA(arete);
    }

    public int getidsommet(){
        return idsommet;
    }

    public int gettype(){
        return typesommet;
    }

    public String getnom(){
        return nomsommet;
    }

    public void setnom(String nnom){
        nomsommet=nnom;
    }
    
    public void settype(int type){
        typesommet=type;
    }
    
    public Arete getArete(int posarete){
        return listareteadjacente.getMaillonLA(posarete).getArete();
    }

    public MaillonLA getFirstMaillon(){
        return listareteadjacente.gettete();
    }

    public ArrayList<Sommet> getvoisins(){
        ArrayList<Sommet> listvoisins=new ArrayList<Sommet>();
        for(MaillonLA pointeur=listareteadjacente.gettete();pointeur!=null;pointeur=pointeur.getsuivant()){
            Arete arete=pointeur.getArete();
            if (arete.getExtremite(0)==this){
                listvoisins.add(arete.getExtremite(1));
            }
            else{
                listvoisins.add(arete.getExtremite(0));
            }
        }
        return listvoisins;
    }

    public ArrayList<Sommet> getvoisinstype(int type){
        ArrayList<Sommet> listvoisins=new ArrayList<Sommet>();
        for(MaillonLA pointeur=listareteadjacente.gettete();pointeur!=null;pointeur=pointeur.getsuivant()){
            Arete arete=pointeur.getArete();
            Sommet sommet;
            if (arete.getExtremite(0)==this){
                sommet=arete.getExtremite(1);
            }
            else{
                sommet=arete.getExtremite(0);
            }
            if(sommet.gettype()==type){
                listvoisins.add(sommet);
            }
        }
        return listvoisins;
    }
    
    public ArrayList<Arete> getallarete(){
        return listareteadjacente.getallarete();
    }
    
    public static int getnbsommet(){
        return nbsommetcree-1;
    }
}
