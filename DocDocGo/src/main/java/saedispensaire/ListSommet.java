package saedispensaire;

import java.util.ArrayList;
import java.util.List;

public class ListSommet {
    public class MaillonLS{
        private final Sommet sommet;
        private MaillonLS suivant;

        public MaillonLS(Sommet sommet,MaillonLS suivant){
            this.sommet=sommet;
            this.suivant=suivant;
        }

        public Sommet getSommet(){
            return sommet;
        }

        public MaillonLS getsuivant(){
            return suivant;
        }

        public void setsuivant(MaillonLS newsuivant){
            suivant=newsuivant;
        }

        /*public delSommet(int idsommet){

        }*/
    }

    private MaillonLS tete;

    public ListSommet(){
        tete=null;
    }

    public void addMaillonLS(Sommet sommet){
        tete=new MaillonLS(sommet,tete);
    }

    /*public boolean delMaillonLS(int idsommet){
        boolean del=false;
        if (tete.getSommet().getidsommet()==idsommet){
            tete=tete.getsuivant();
            del=true;
        }
        else{
            MaillonLS ptr=tete;
            while (ptr.getsuivant()!=null && del!=true){
                if (ptr.getsuivant().getSommet().getidsommet()==idsommet){
                    ptr.setsuivant(ptr.getsuivant().getsuivant());
                    del=true;
                }
                ptr=ptr.getsuivant();
            }
        }
        return del;
    }*/

    public MaillonLS getMaillonLS(int idsommet){
        MaillonLS maillon=null;
        MaillonLS ptr=tete;
        while (ptr!=null && maillon==null){
            if (ptr.getSommet().getidsommet()==(idsommet)){
                maillon=ptr;
            }
            ptr=ptr.getsuivant();
        }
        return maillon;
    }

    public MaillonLS gettete(){
        return tete;
    }

    public List<Sommet> getsommettype(int type){
        List<Sommet> listsommet=new ArrayList<>();
        Sommet sommet;
        for(MaillonLS pointeur=tete;pointeur!=null;pointeur=pointeur.suivant){
            sommet=pointeur.getSommet();
            if(sommet.gettype()==type)listsommet.add(sommet);
        }
        return listsommet;
    }

    public int getnbsommettype(int type){
        return getsommettype(type).size();
    }

    public List<Sommet> listsommet(){
        List<Sommet> listsommet=new ArrayList<>();//1
        Sommet sommet;
        for(MaillonLS pointeur=tete;pointeur!=null;pointeur=pointeur.suivant){//(n+3)*(1+complexite(arraylist.add))
            sommet=pointeur.getSommet();//1
            listsommet.add(sommet);//complexite(arraylist.add)
        }
        return listsommet;
    }

    public List<Sommet> listsommettrie(){
        List<Sommet> listsommet=listsommet();
        return listsommet;
    }
}
