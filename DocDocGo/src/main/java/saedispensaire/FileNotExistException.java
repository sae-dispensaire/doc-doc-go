
package saedispensaire;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FileNotExistException extends Exception{
    public FileNotExistException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Selected file doesn't exist","Error",JOptionPane.WARNING_MESSAGE);
    }
}
