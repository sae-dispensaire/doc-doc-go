
package saedispensaire;

public class MaillonDijkstra {
    public Sommet sommetratache;
    public double fiabilite;
    public MaillonDijkstra antecedent;
    public double duree;
    public double distance;
    public boolean relache;
    
    public MaillonDijkstra(){
        antecedent=null;
        relache=false;
    }
}
