
package saedispensaire;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class FileNotInitialisedException extends Exception{
    public FileNotInitialisedException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Selected file has not been initialised","Error",JOptionPane.WARNING_MESSAGE);
    }
}
