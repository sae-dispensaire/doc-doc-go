package saedispensaire;

import java.util.ArrayList;

public class ListArete {
    
    public class MaillonLA{
        private final Arete arete;
        private MaillonLA suivant;

        public MaillonLA(Arete arete,MaillonLA suivant){
            this.arete=arete;
            this.suivant=suivant;
        }

        public Arete getArete(){
            return arete;
        }

        public MaillonLA getsuivant(){
            return suivant;
        }

        public void setsuivant(MaillonLA newsuivant){
            suivant=newsuivant;
        }
    }

    private MaillonLA tete;

    public ListArete(){
        tete=null;
    }

    public void addMaillonLA(Arete arete){
        tete=new MaillonLA(arete,tete);
    }

    /*public boolean delMaillonLA(int idarete){
        boolean del=false;
        if (tete.getArete().getidarete()==idarete){
            tete=tete.getsuivant();
            del=true;
        }
        else{
            MaillonLA ptr=tete;
            while (ptr.getsuivant()!=null && del!=true){
                if (ptr.getsuivant().getArete().getidarete()==idarete){
                    ptr.setsuivant(ptr.getsuivant().getsuivant());
                    del=true;
                }
                ptr=ptr.getsuivant();
            }
        }
        return del;
    }*/

    public MaillonLA getMaillonLA(int posarete){
        MaillonLA maillon=null;
        MaillonLA ptr=tete;
        for(int i=0;i<posarete;i++){
            ptr=ptr.getsuivant();
        }
        maillon=ptr;
        /*while (ptr!=null && maillon==null){
            if (ptr.getArete().getidarete()==idarete){
                maillon=ptr;
            }
            ptr=ptr.getsuivant();
        }*/
        return maillon;
    }

    public MaillonLA gettete(){
        return tete;
    }

    public ArrayList<Arete> getallarete(){//(n+3)*complexite(arraylist.add)+1
        ArrayList<Arete> listarete=new ArrayList<Arete>();//1
        for(MaillonLA pointeur=tete;pointeur!=null;pointeur=pointeur.suivant){//(n+3)*complexite(arraylist.add)
            listarete.add(pointeur.getArete());
        }
        return listarete;
    }
}
