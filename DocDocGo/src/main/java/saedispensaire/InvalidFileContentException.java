
package saedispensaire;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author hugom
 */
public class InvalidFileContentException extends Exception{
    String filename;
    public InvalidFileContentException(String filename){
        super();
        this.filename=filename;
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : File "+filename+" content is invalid, try to load an other file or correct the invalid data it contains.","Error",JOptionPane.WARNING_MESSAGE);
    }
}
