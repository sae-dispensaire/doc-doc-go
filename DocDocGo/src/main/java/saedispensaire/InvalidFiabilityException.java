
package saedispensaire;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author hugom
 */
public class InvalidFiabilityException extends Exception{
    public InvalidFiabilityException(){
        super();
    }
    
    public void errorFrame(JFrame frame){
        JOptionPane.showMessageDialog(frame,"Error : Fiability must be between 1 and 0. 1 representing a totally reliable path","Error",JOptionPane.WARNING_MESSAGE);
    }
}
