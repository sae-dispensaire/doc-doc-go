package saedispensaire;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;
import saedispensaire.ListArete.MaillonLA;
import saedispensaire.ListSommet.MaillonLS;

public class Graphe {
    private ListSommet listsommetgraphe;
    private ListArete listaretegraphe;
    
    public Graphe(){
        listsommetgraphe=new ListSommet();
        listaretegraphe=new ListArete();
        Arete.resetnbarete();
        Sommet.resetnbsommet();
    }
    
    //FONCTIONS
    
    public void readGraphe(File fichieradjacence,File fichiersuccesseur) throws InvalidFileContentException/*throws FileNotFoundException*/{
        Scanner parcoursfichier=null;
        FileReader file=null;
        String ligne=null;
        String[] lignesplit=null;
        ArrayList<ArrayList<String>> listadjacence=new ArrayList<ArrayList<String>>();
        ArrayList<LinkedList<Integer>> listsuccesseur=new ArrayList<LinkedList<Integer>>();
        
        //lecture successeur
        try{
            file=new FileReader(fichiersuccesseur);
            parcoursfichier=new Scanner(file);
            int i=0;
            
            while (parcoursfichier.hasNextLine()){
                LinkedList<Integer> lignelist=null;
                ligne=parcoursfichier.nextLine();
                lignesplit=ligne.split(";");
                
                int[] tableausuccesseur=new int[lignesplit.length];
                
                listsuccesseur.add(new LinkedList<Integer>());
                lignelist=listsuccesseur.get(i);
                
                int j=0;
                for (String successeur:lignesplit){
                    if(estInt(successeur)){
                        tableausuccesseur[j]=Integer.parseInt(successeur);
                        j++;
                    }
                }
                
                tableausuccesseur=trisuccesseur(tableausuccesseur);
                
                for (int successeur:tableausuccesseur){
                    if (successeur!=0){
                        lignelist.add(successeur);
                    }
                }
                
                i++;
            }
            file.close();
        }
        catch(FileNotFoundException fnte){
            System.out.println("Le fichier de la liste des successeurs est introuvable.");
        } 
        catch (IOException ioxe) {
            System.out.println("Erreur durant la lecture de la liste des successeurs");
        }
        
        //lecture adjacent
        try{
            file=new FileReader(fichieradjacence);
            parcoursfichier=new Scanner(file);
            int i=0;
            
            while (parcoursfichier.hasNextLine()){
                ArrayList<String> lignelist=null;
                ligne=parcoursfichier.nextLine();
                ligne=ligne.replace(" ", "");
                ligne=ligne.replaceAll(String.valueOf('\n'),"");
                lignesplit=ligne.split(";");
                
                if(lignesplit[0].substring(0,1).equals("/")==false){
                    listadjacence.add(new ArrayList<String>());
                    lignelist=listadjacence.get(i);
                
                    for (String adjacence:lignesplit){
                        if(adjacence.equals("0")==false){
                            lignelist.add(adjacence);
                        }
                    }

                    i++;
                }
            }
            file.close();
        }
        catch(FileNotFoundException fnte){
            System.out.println("Le fichier de la liste d'adjacence est introuvable.");
        } 
        catch (IOException ioxe) {
            System.out.println("Erreur durant la lecture de la liste d'adjacence");
        }
        
        //création sommets
        Iterator<ArrayList<String>> iterateuradjacence =listadjacence.iterator();
        int i=0;
        listsommetgraphe=new ListSommet();
        while (iterateuradjacence.hasNext()){
            ArrayList<String> lignelist=null;
            lignelist=iterateuradjacence.next();
            
            int type=-1;
            switch(lignelist.get(1).toLowerCase()){
                case "m":
                    type=1;
                    break;
                case "n":
                    type=2;
                    break;
                case "o":
                    type=3;
                    break;
                default:
                    System.out.println("Type ivalide");
            }
            
            listsommetgraphe.addMaillonLS(new Sommet(lignelist.get(0),type));
             
            lignelist.remove(0);
            lignelist.remove(0);
            
            i++;
            
        }
        
        //création arrêtes
        Iterator<LinkedList<Integer>> iterateursuccesseur =listsuccesseur.iterator();
        i=0;
        listaretegraphe=new ListArete();
        try{
            while (iterateursuccesseur.hasNext()){
                LinkedList<Integer> lignelist=null;
                lignelist=iterateursuccesseur.next();
                Sommet origine=listsommetgraphe.getMaillonLS(i+1).getSommet();
                while(lignelist.isEmpty()==false){
                    int successeur=lignelist.getFirst();
                    Sommet destination=listsommetgraphe.getMaillonLS(successeur).getSommet();
                    String[] carArete=listadjacence.get(i).get(0).split(",");
                    if(Double.parseDouble(carArete[1])<0){
                        throw new NegativeDistanceException();
                    }
                    else if(Double.parseDouble(carArete[2])<0){
                        throw new NegativeDurationException();
                    }
                    Arete arete=origine.addArete(Double.parseDouble(carArete[0])/10,Double.parseDouble(carArete[1]),Double.parseDouble(carArete[2]),destination);
                    listaretegraphe.addMaillonLA(arete);
                    listadjacence.get(i).remove(0);
                    listadjacence.get(successeur-1).remove(0);
                    listsuccesseur.get(successeur-1).removeFirst();
                    lignelist.removeFirst();
                }
                i++;
            }
        }
        catch(NegativeDistanceException nde){
            nde.errorFrame(null);
            throw new InvalidFileContentException(fichieradjacence.getName());
        }
        catch(NegativeDurationException nde){
            nde.errorFrame(null);
            throw new InvalidFileContentException(fichieradjacence.getName());
        }
    }
    
    public void saveGraphe(File fichieradjacence,File fichiersuccesseur){
        FileWriter file=null;
        
        List<Sommet> listsommet=listsommetgraphe.listsommet();
        Sommet sommet;
        
        //save successeur
        try{
            file=new FileWriter(fichiersuccesseur);
            for (int i=listsommet.size()-1;i>=0;i--){
                sommet=listsommet.get(i);
                String ligne=sommet.getnom();

                ArrayList listarete=sommet.getallarete();
                for(int y=listarete.size()-1;y>=0;y--){
                    Arete arete =(Arete) listarete.get(y);
                    int successeur;
                    if(arete.getExtremite(0)==sommet){
                        successeur=arete.getExtremite(1).getidsommet();
                    }
                    else{
                        successeur=arete.getExtremite(0).getidsommet();
                    }
                    ligne=ligne+";"+successeur;
                }
                
                file.write(ligne+System.lineSeparator());
            }
            
            file.close();
        }
        catch (IOException ioxe) {
            System.out.println("Erreur durant la sauvegarde de la liste des successeurs");
        }
        //save adjacent
        try{
            file=new FileWriter(fichieradjacence);
            ArrayList<String> ligneadjacencelist=new ArrayList<String>();
            ArrayList<List> listareteparsommet=new ArrayList<List>();
            
            for (int i=listsommet.size()-1;i>=0;i--){
                sommet=listsommet.get(i);
                String type=null;
                switch(sommet.gettype()){
                    case 1:
                        type="M";
                        break;
                    case 2:
                        type="N";
                        break;
                    case 3:
                        type="O";
                        break;
                }
                ligneadjacencelist.add(sommet.getnom()+";"+type);
                
                listareteparsommet.add(sommet.getallarete());
            }
            int i=0;
            while(i<listareteparsommet.size()){
                List listarete=listareteparsommet.get(i);
                while(!listarete.isEmpty()){
                    Arete arete=(Arete)listarete.get(listarete.size()-1);
                    
                    int extremite=arete.getExtremite(0).getidsommet();
                    List listareteextremite;
                    if(extremite==i+1){
                        extremite=arete.getExtremite(1).getidsommet();
                    }
                    
                    for(int y=0;y<ligneadjacencelist.size();y++){
                        if(y==extremite-1||y==i){
                            ligneadjacencelist.set(y,ligneadjacencelist.get(y)+";"+arete.getfiable()*10+","+arete.getdist()+","+arete.getduree());
                        }
                        else{
                            ligneadjacencelist.set(y,ligneadjacencelist.get(y)+";0");
                        }
                    }
                    
                    listareteextremite=listareteparsommet.get(extremite-1);
                    listareteextremite.remove(listareteextremite.size()-1);
                    listarete.remove(arete);
                }
                i++;
            }
            
            for(String ligne:ligneadjacencelist){
                file.write(ligne+System.lineSeparator());
            }
            file.close();
        }
        catch (IOException ioxe) {
            System.out.println("Erreur durant la sauvegarde de la matrice d'adjacence");
        }
        JOptionPane.showMessageDialog(null,"Files saved successfuly","Success",JOptionPane.INFORMATION_MESSAGE);
    }
    
    private int[] trisuccesseur(int[] successeurs){
        int len=successeurs.length;
        int i=len-1;
        while (0<i){
            
            int max=i;
            int j=i-1;
            while (0<=j){
                
                if (successeurs[j]>successeurs[max]){
                    max=j;
                }
                j--;
            }
            if (max!=i){
                int temp=successeurs[i];
                successeurs[i]=successeurs[max];
                successeurs[max]=temp;
            }
            i--;
        }
        return successeurs;
    }
    
    private static boolean estInt(String chaine){
        try{
            Integer.parseInt(chaine);
        }
        catch(NumberFormatException nfe){
            return false;
        }
        return true;
    }
    
    public void afficherGraphe(){
        MaillonLS maillonsommet=listsommetgraphe.gettete();
        while(maillonsommet!=null){
            System.out.print(maillonsommet.getSommet().getnom()+" : ");
            MaillonLA maillon=maillonsommet.getSommet().getFirstMaillon();
            while (maillon!=null){
                Arete arete=maillon.getArete();
                maillon=maillon.getsuivant();
                System.out.println("\t"+arete.getExtremite(0).getnom()+"-"+arete.getExtremite(1).getnom()+":"+arete.getdist()+"km"+";"+arete.getduree()+"min"+";"+arete.getfiable()*100+"%");
            }
            maillonsommet=maillonsommet.getsuivant();
        }
    }
    
    public Sommet getsommet(int idsommet){
        Sommet sommet=listsommetgraphe.getMaillonLS(idsommet).getSommet();
        return sommet;
    }
    
    public static int getnbarete(){
        return Arete.getnbarete();
    }

    public List<Arete> getallarete(){
        return listaretegraphe.getallarete();
    }
    
    public List<Sommet> getallsommet(){
        return listsommetgraphe.listsommet();
    }
    
    public List<Sommet> getallsommettype(int type){
        return listsommetgraphe.getsommettype(type);
    }
    
    public int getnbsommettype(int type){
        return listsommetgraphe.getnbsommettype(type);
    }
    
    public List<Sommet> getallsommettrie(){
        List<Sommet> listtrie=listsommetgraphe.getsommettype(3);
        listtrie.addAll(listsommetgraphe.getsommettype(2));
        listtrie.addAll(listsommetgraphe.getsommettype(1));
        return listtrie;
    }
    
    public List<Sommet> getsommetcommuns(Sommet sommet1,Sommet sommet2){
        List<Sommet> listcommuns=new ArrayList<Sommet>();
        List<Sommet> listvoisinssommet1,listvoisinssommet2;
        listvoisinssommet1=sommet1.getvoisins();
        listvoisinssommet2=sommet2.getvoisins();
        Iterator<Sommet> iterateursommet1=listvoisinssommet1.iterator();
        while(iterateursommet1.hasNext()){
            Sommet voisinsommet1=iterateursommet1.next();
            boolean trouve=false;
            
            if(sommet2!=voisinsommet1){
                Iterator<Sommet> iterateursommet2=listvoisinssommet2.iterator();
                while(iterateursommet2.hasNext()&&trouve==false){
                    Sommet voisinsommet2=iterateursommet2.next();
                    
                    if(sommet1!=voisinsommet2&&voisinsommet1==voisinsommet2){
                        trouve=true;
                        listcommuns.add(voisinsommet1);
                    }
                }
            }
        }
        return listcommuns;
    }
    
    public List<Sommet> getsommetsommunstype(Sommet sommet1,Sommet sommet2,int type){
        List<Sommet> listcommuns=new ArrayList<Sommet>();
        List<Sommet> listvoisinssommet1,listvoisinssommet2;
        listvoisinssommet1=sommet1.getvoisins();
        listvoisinssommet2=sommet2.getvoisins();
        Iterator<Sommet> iterateursommet1=listvoisinssommet1.iterator();
        while(iterateursommet1.hasNext()){
            Sommet voisinsommet1=iterateursommet1.next();
            boolean trouve=false;
            
            if(sommet2!=voisinsommet1&&type==voisinsommet1.gettype()){
                Iterator<Sommet> iterateursommet2=listvoisinssommet2.iterator();
                while(iterateursommet2.hasNext()&&trouve==false){
                    Sommet voisinsommet2=iterateursommet2.next();
                    
                    if(sommet1!=voisinsommet2&&voisinsommet1==voisinsommet2){
                        trouve=true;
                        listcommuns.add(voisinsommet1);
                    }
                }
            }
        }
        return listcommuns;
    }
    
    public ComparaisonSommet comparersommets(Sommet sommet1,Sommet sommet2,int type){
        ComparaisonSommet comparaison=new ComparaisonSommet(sommet1,sommet2);
        
        //boucle sommet1
        List<Sommet> listsommet=getallsommettype(type);
        listsommet.removeAll(sommet1.getvoisinstype(type));
        for(Sommet sommettype:listsommet){
            if(sommettype!=sommet1 && sommets2distance(sommet1,sommettype)==false){
                comparaison.nbtype1++;
            }
        }
        
        //boucle sommet2
        listsommet=getallsommettype(type);
        listsommet.removeAll(sommet2.getvoisinstype(type));
        for(Sommet sommettype:listsommet){
            if(sommettype!=sommet2 && sommets2distance(sommet2,sommettype)==false){
                comparaison.nbtype2++;
            }
        }
        
        return comparaison;
    }
    
    public Chemin recherchecheminrapide(Sommet origine,Sommet destination){
        //4ncube+48ncarre-22n+41 complexite en temps dans le pire des cas
        Chemin trajet=new Chemin();//5
        
        ArrayList<Sommet> listsommet=(ArrayList<Sommet>)listsommetgraphe.listsommet();//n+4+(n+3)(complexite(arraylist.add))
        ArrayList<MaillonDijkstra>tabdijkstra=new ArrayList<MaillonDijkstra>();//1
        MaillonDijkstra maillontraite=null;//1
        
        for(Sommet sommet:listsommet){//10n+n(complexite(arraylist.add()))
           MaillonDijkstra maillon=new MaillonDijkstra();//3
           maillon.sommetratache=sommet;//1
           if (sommet==origine){//5
               maillon.fiabilite=1;
               maillon.distance=0;
               maillon.duree=0;
               maillontraite=maillon;
           }
           else{//4
               maillon.fiabilite=1;
               maillon.distance=Double.POSITIVE_INFINITY;
               maillon.duree=Double.POSITIVE_INFINITY;
           }
           tabdijkstra.add(maillon);//complexite(arraylist.add)
        }
        
        while (maillontraite!=null){//-35n+41ncarre+3ncube+(ncarre+3n)(complexite(arraylist.add))+(ncube+6ncarre-7n)(complexite(arraylist.get))
            maillontraite.relache=true;//1
            
            ArrayList<Arete> arcrelacher=maillontraite.sommetratache.getallarete();//2+(n+3)*complexite(arraylist.add)
            
            for(Arete arc:arcrelacher){//3ncarre+36n-39+(ncarre+6n-7)(complexite(arraylist.get))
                Sommet extremite;
                if (arc.getExtremite(0)==maillontraite.sommetratache){//4
                    extremite=arc.getExtremite(1);//2
                }
                else{//4
                    extremite=arc.getExtremite(0);//2
                }
                
                MaillonDijkstra traite=null;//1
                for(int i=0;traite==null&&i<tabdijkstra.size();i++){//(n+7)*(3+complexite(arraylist.get)
                    MaillonDijkstra maillon=tabdijkstra.get(i);//1+complexite(arraylist.get)
                    if(maillon.sommetratache==extremite){//1
                        traite=maillon;//1
                    }
                }
                
                if(traite!=null&&traite.relache==false){//1
                    double vmoyenne=(maillontraite.distance+arc.getdist())/(maillontraite.duree+arc.getduree());//3
                    if (traite.distance==Double.POSITIVE_INFINITY||(traite.distance/traite.duree)<vmoyenne){//3
                        traite.antecedent=maillontraite;//1
                        traite.fiabilite=maillontraite.fiabilite*arc.getfiable();//1
                        traite.distance=maillontraite.distance+arc.getdist();//2
                        traite.duree=maillontraite.duree+arc.getduree();//2
                    }
                }
            }
            
            maillontraite=null;//1
            for(MaillonDijkstra maillon:tabdijkstra){//n*5
                if(maillon.relache==false&&maillon.distance!=Double.POSITIVE_INFINITY){//1
                    if(maillontraite==null||(maillon.distance/maillon.duree)>(maillontraite.distance/maillontraite.duree)){//3
                        maillontraite=maillon;//1
                    }
                }
            }
        }
        
        MaillonDijkstra maillondestination=null;//1
        for(int i=0;maillondestination==null&&i<tabdijkstra.size();i++){//2n+12+(2n+12)(complexite(arraylist.get)))
            if(tabdijkstra.get(i).sommetratache==destination){//complexite(arraylist.get))+1
                maillondestination=tabdijkstra.get(i);//complexite(arraylist.get))+1
            }
        }
        
        if(maillondestination!=null){//1
            trajet.distance=maillondestination.distance;//1
            trajet.duree=maillondestination.duree;//1
            trajet.fiabilite=maillondestination.fiabilite;//1
            while(maillondestination!=null){//1
                trajet.meilleurchemin.add(maillondestination.sommetratache);//complexite(arraylist.add))
                maillondestination=maillondestination.antecedent;//1
            }
        }
        return trajet;
    }
    
    public Chemin recherchecheminfiable(Sommet origine,Sommet destination){
        //4ncube+48ncarre-22n+41 complexite en temps dans le pire des cas
        Chemin trajet=new Chemin();//5
        
        ArrayList<Sommet> listsommet=(ArrayList<Sommet>)listsommetgraphe.listsommet();//n+4+(n+3)(complexite(arraylist.add))
        ArrayList<MaillonDijkstra>tabdijkstra=new ArrayList<MaillonDijkstra>();//1
        MaillonDijkstra maillontraite=null;//1
        
        for(Sommet sommet:listsommet){//10n+n(complexite(arraylist.add()))
           MaillonDijkstra maillon=new MaillonDijkstra();//3
           maillon.sommetratache=sommet;//1
           if (sommet==origine){//5
               maillon.fiabilite=1;
               maillon.distance=0;
               maillon.duree=0;
               maillontraite=maillon;
           }
           else{//4
               maillon.fiabilite=1;
               maillon.distance=Double.POSITIVE_INFINITY;
               maillon.duree=Double.POSITIVE_INFINITY;
           }
           tabdijkstra.add(maillon);//complexite(arraylist.add)
        }
        
        while (maillontraite!=null){//-35n+41ncarre+3ncube+(ncarre+3n)(complexite(arraylist.add))+(ncube+6ncarre-7n)(complexite(arraylist.get))
            maillontraite.relache=true;//1
            
            ArrayList<Arete> arcrelacher=maillontraite.sommetratache.getallarete();//2+(n+3)*complexite(arraylist.add)
            
            for(Arete arc:arcrelacher){//3ncarre+36n-39+(ncarre+6n-7)(complexite(arraylist.get))
                Sommet extremite;
                if (arc.getExtremite(0)==maillontraite.sommetratache){//4
                    extremite=arc.getExtremite(1);//2
                }
                else{//4
                    extremite=arc.getExtremite(0);//2
                }
                
                MaillonDijkstra traite=null;//1
                for(int i=0;traite==null&&i<tabdijkstra.size();i++){//(n+7)*(3+complexite(arraylist.get)
                    MaillonDijkstra maillon=tabdijkstra.get(i);//1+complexite(arraylist.get)
                    if(maillon.sommetratache==extremite){//1
                        traite=maillon;//1
                    }
                }
                
                if(traite!=null){//1
                    double fiabilitetest=maillontraite.fiabilite*arc.getfiable();//3
                    if (traite.distance==Double.POSITIVE_INFINITY||traite.fiabilite<fiabilitetest){//3
                        traite.antecedent=maillontraite;//1
                        traite.fiabilite=fiabilitetest;//1
                        traite.distance=maillontraite.distance+arc.getdist();//2
                        traite.duree=maillontraite.duree+arc.getduree();//2
                    }
                }
            }
            
            maillontraite=null;//1
            for(MaillonDijkstra maillon:tabdijkstra){//n*5
                System.out.println(maillon.relache==true?"vrai":"faux");
                if(maillon.relache==false){//1
                    if(maillontraite==null||(maillon.fiabilite>maillontraite.fiabilite&&maillon.distance!=Double.POSITIVE_INFINITY)){//3
                        maillontraite=maillon;//1
                    }
                }
            }
        }
        
        MaillonDijkstra maillondestination=null;//1
        for(int i=0;maillondestination==null&&i<tabdijkstra.size();i++){//2n+12+(2n+12)(complexite(arraylist.get)))
            if(tabdijkstra.get(i).sommetratache==destination){//complexite(arraylist.get))+1
                maillondestination=tabdijkstra.get(i);//complexite(arraylist.get))+1
            }
        }
        
        if(maillondestination!=null){//1
            trajet.distance=maillondestination.distance;//1
            trajet.duree=maillondestination.duree;//1
            trajet.fiabilite=maillondestination.fiabilite;//1
            while(maillondestination!=null){//1
                trajet.meilleurchemin.add(maillondestination.sommetratache);//complexite(arraylist.add))
                maillondestination=maillondestination.antecedent;//1
            }
        }
        
        return trajet;
    }
    
    public static String getcomplexitecheminfiable(){
        return "4ncube+48ncarre-22n+41";
    }
    
    public boolean sommets2distance(Sommet sommet1,Sommet sommet2){
        boolean distance=false;
        List<Sommet> listvoisinssommet1,listvoisinssommet2;
        listvoisinssommet1=sommet1.getvoisins();
        listvoisinssommet2=sommet2.getvoisins();
        Iterator<Sommet> iterateursommet1=listvoisinssommet1.iterator();
        while(iterateursommet1.hasNext()&&distance==false){
            Sommet voisinsommet1=iterateursommet1.next();
            boolean trouve=false;
            
            if(sommet2!=voisinsommet1){
                Iterator<Sommet> iterateursommet2=listvoisinssommet2.iterator();
                while(iterateursommet2.hasNext()&&trouve==false){
                    Sommet voisinsommet2=iterateursommet2.next();
                    
                    if(sommet1!=voisinsommet2&&voisinsommet1==voisinsommet2){
                        trouve=true;
                        distance=true;
                    }
                }
            }
        }
        return distance;
    }
    
    public void setnomsommet(int idsommet,String newnom){
        Sommet sommet=listsommetgraphe.getMaillonLS(idsommet).getSommet();
        sommet.setnom(newnom);
    }
    
    public void setfiabilite(int posarete,double newfiabilite)throws InvalidFiabilityException{
        if (newfiabilite>1||newfiabilite<0){
            
        }
    }
    
    public void setdistance(int posarete,double newdistance)throws NegativeDistanceException{
        if (newdistance<0){
            
        }
    }
    
    public void setduree(int posarete,double newduree)throws NegativeDurationException{
        if (newduree<0){
            throw new NegativeDurationException();
        }
    }
}
