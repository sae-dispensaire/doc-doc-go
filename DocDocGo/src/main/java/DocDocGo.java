
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import static java.awt.Component.CENTER_ALIGNMENT;
import java.awt.Desktop;
import java.io.File;
import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import saedispensaire.FileNotExistException;
import saedispensaire.Graphe;
import saedispensaire.WrongExtensionException;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import saedispensaire.Arete;
import saedispensaire.CancelSelection;
import saedispensaire.Chemin;
import saedispensaire.ComparaisonSommet;
import saedispensaire.FileNotInitialisedException;
import saedispensaire.InvalidFileContentException;
import saedispensaire.Sommet;

public class DocDocGo {
    static Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
    static Graphe graphe;
    static JPanel content = null;
    static CardLayout layoutmainframe = new CardLayout();
    static File fileadjacence;
    static File filesuccesseur;
    
    public static void main(String[] args) {
        InterfaceSelection interfaceprincipale = new InterfaceSelection();
        
        //graphe=new Graphe();
        fileadjacence=null;
        filesuccesseur=null;
        /*try{
            File[] temp=openfichier(null);
            if (temp!=null){
                fileadjacence = temp[0];
                filesuccesseur = temp[1];
                System.out.println(fileadjacence);
                
                graphe.afficherGraphe();
                System.out.println(graphe.getsommetcommuns(graphe.getsommet(3),graphe.getsommet(1)));
                System.out.println(graphe.sommets2distance(graphe.getsommet(3),graphe.getsommet(1))==true);
                System.out.println(graphe.getsommet(1).getvoisins());
                System.out.println(graphe.recherchecheminfiable(graphe.getsommet(1),graphe.getsommet(2)));
                System.out.println(graphe.getcomplexitecheminfiable());
                System.out.println("--------------------------------------------------------");
                System.out.println(graphe.recherchecheminrapide(graphe.getsommet(1),graphe.getsommet(2)));
                save(fileadjacence,filesuccesseur);

                }
                
        }
        catch(InvalidFileContentException ifce){
                    ifce.errorFrame(null);
                }*/
    }
    
    private static void reload(File fichieradjacence,File fichiersuccesseur) throws InvalidFileContentException{
        try{
            if(fichieradjacence==null||fichiersuccesseur==null){
                throw new FileNotInitialisedException();
            }
            graphe=new Graphe();
            graphe.readGraphe(fichieradjacence, fichiersuccesseur);
        }
        catch(FileNotInitialisedException fnie){
            fnie.errorFrame(null);
        }
    }
    
    private static File[] openfichier(JFrame frame) throws InvalidFileContentException{
        graphe=new Graphe();
        File[] fichiers=new File[2];
        try{
            fichiers[0]=selectmatriceadjacence(frame);
            fichiers[1]=selectlistesuccesseur(frame);
            graphe.readGraphe(fichiers[0], fichiers[1]);
        }
        catch(CancelSelection cs){
            fichiers=null;
        }
        return fichiers;
    }
    
    private static void save(File fichieradjacence,File fichiersuccesseur){
        try{
            if(fichieradjacence==null||fichiersuccesseur==null){
                throw new FileNotInitialisedException();
            }
            graphe.saveGraphe(fichieradjacence,fichiersuccesseur );
        }
        catch(FileNotInitialisedException fnie){
            fnie.errorFrame(null);
        }
    }
    
    private static File[] savefichier(JFrame frame){
        File[] fichiers=new File[2];
        try{
            fichiers[0]=selectsavematriceadjacence(frame);
            fichiers[1] = selectsavelistesuccesseur(frame);
            graphe.saveGraphe(fichiers[0],fichiers[1] );
        }
        catch(CancelSelection cs){
        }
        return fichiers;
    }
    
    private static File selectmatriceadjacence(JFrame frame)throws CancelSelection{
        File fichieradjacence=null;
        boolean valid=false;
        
        while (fichieradjacence==null || valid!=true){
            try{
                JFileChooser filechooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV & TXT", "csv", "txt");

                Action details = filechooser.getActionMap().get("viewTypeDetails");
                details.actionPerformed(null);
                
                filechooser.setFileFilter(filter);
                filechooser.setAcceptAllFileFilterUsed(false);
                filechooser.setVisible(true);
                int longueur=tailleEcran.width * 1/2;
                int largeur=tailleEcran.height * 1/2;
                filechooser.setPreferredSize(new Dimension(longueur,largeur));

                filechooser.setDialogTitle("Open adjacency");
                filechooser.setDialogType(JFileChooser.OPEN_DIALOG);
                int valider=filechooser.showOpenDialog(frame);

                if(valider == JFileChooser.APPROVE_OPTION) {
                    fichieradjacence = filechooser.getSelectedFile();
                 
                    if(!fichieradjacence.exists()){
                        throw new FileNotExistException();
                    }
                    String extension = fichieradjacence.getName();
                    extension = extension.substring(extension.lastIndexOf('.')+1);
                    extension = extension.replaceAll(String.valueOf('\n'),"");
                    if(!extension.contentEquals("csv")&&!extension.contentEquals("txt")){
                        throw new WrongExtensionException();
                    }
                    valid = true;
                    System.out.println("You chose to open this file: " + fichieradjacence.getName());
                    JOptionPane.showMessageDialog(filechooser,"File selected successfuly","Success",JOptionPane.INFORMATION_MESSAGE);
                }
                else if( valider==JFileChooser.CANCEL_OPTION){
                    throw new CancelSelection();
                }   
            }
            catch(FileNotExistException fnee){
                fnee.errorFrame(frame);
            }
            catch(WrongExtensionException wee){
                wee.errorFrame(frame);
            }
        }
        return fichieradjacence;
    }

    private static File selectlistesuccesseur(JFrame frame)throws CancelSelection{
        File fichiersuccesseur =null;
        boolean valid = false;
        
        while (fichiersuccesseur==null || valid != true){
            try{
                JFileChooser filechooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV & TXT", "csv", "txt");
                filechooser.setFileFilter(filter);
                
                Action details = filechooser.getActionMap().get("viewTypeDetails");
                details.actionPerformed(null);

                filechooser.setAcceptAllFileFilterUsed(false);
                filechooser.setVisible(true);
                filechooser.setDialogTitle("Open successor");
                filechooser.setDialogType(JFileChooser.OPEN_DIALOG);
                int longueur=tailleEcran.width * 1/2;
                int largeur=tailleEcran.height * 1/2;
                filechooser.setPreferredSize(new Dimension(longueur,largeur));

                int valider = filechooser.showOpenDialog(frame);
                if(valider == JFileChooser.APPROVE_OPTION) {
                    fichiersuccesseur = filechooser.getSelectedFile();
                    if(!fichiersuccesseur.exists()){
                       throw new FileNotExistException(); 
                    }
                    String extension = fichiersuccesseur.getName();
                    extension = extension.substring(extension.lastIndexOf('.')+1);
                    extension = extension.replaceAll(String.valueOf('\n'),"");
                    if(!extension.contentEquals("csv")&&!extension.contentEquals("txt")){
                        throw new WrongExtensionException();
                    }
                    valid = true;
                    System.out.println("You chose to open this file: " + fichiersuccesseur.getName());
                        JOptionPane.showMessageDialog(filechooser,"File selected successfuly","Success",JOptionPane.INFORMATION_MESSAGE);
                }
                else if(valider == JFileChooser.CANCEL_OPTION){
                    throw new CancelSelection();
                }
            }
            catch(FileNotExistException fnee){
                fnee.errorFrame(frame);
            }
            catch(WrongExtensionException wee){
                wee.errorFrame(frame);
            }
        }
        return fichiersuccesseur;
    }
    
    private static File selectsavelistesuccesseur(JFrame frame)throws CancelSelection{
        File fichiersuccesseur =null;
        boolean valid = false;
        
        while (fichiersuccesseur==null || valid != true){
            try{
                JFileChooser filechooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV & TXT", "csv", "txt");
                filechooser.setFileFilter(filter);
                
                Action details = filechooser.getActionMap().get("viewTypeDetails");
                details.actionPerformed(null);

                filechooser.setAcceptAllFileFilterUsed(false);
                filechooser.setVisible(true);
                filechooser.setDialogTitle("Save successor");
                filechooser.setDialogType(JFileChooser.OPEN_DIALOG);
                int longueur=tailleEcran.width * 1/2;
                int largeur=tailleEcran.height * 1/2;
                filechooser.setPreferredSize(new Dimension(longueur,largeur));

                int valider = filechooser.showSaveDialog(frame);
                if(valider == JFileChooser.APPROVE_OPTION) {
                    fichiersuccesseur = filechooser.getSelectedFile();
                    
                    String extension = fichiersuccesseur.getName();
                    extension = extension.substring(extension.lastIndexOf('.')+1);
                    extension = extension.replaceAll(String.valueOf('\n'),"");
                    if(!extension.contentEquals("csv")&&!extension.contentEquals("txt")){
                        throw new WrongExtensionException();
                    }
                    if(!fichiersuccesseur.exists()){
                        fichiersuccesseur.createNewFile();
                    }
                    valid = true;
                    System.out.println("You save as this file: " + fichiersuccesseur.getName());
                        JOptionPane.showMessageDialog(filechooser,"File save successfuly","Success",JOptionPane.INFORMATION_MESSAGE);
                }
                else if(valider == JFileChooser.CANCEL_OPTION){
                    throw new CancelSelection();
                }
            }
            catch(WrongExtensionException wee){
                wee.errorFrame(frame);
            }
            catch(IOException IOE){
                
            }
        }
        return fichiersuccesseur;
    }
    
    private static File selectsavematriceadjacence(JFrame frame)throws CancelSelection{
        File fichieradjacence =null;
        boolean valid = false;
        
        while (fichieradjacence==null || valid != true){
            try{
                JFileChooser filechooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV & TXT", "csv", "txt");
                filechooser.setFileFilter(filter);
                
                Action details = filechooser.getActionMap().get("viewTypeDetails");
                details.actionPerformed(null);

                filechooser.setAcceptAllFileFilterUsed(false);
                filechooser.setVisible(true);
                filechooser.setDialogTitle("Save Adjacence");
                filechooser.setDialogType(JFileChooser.OPEN_DIALOG);
                int longueur=tailleEcran.width * 1/2;
                int largeur=tailleEcran.height * 1/2;
                filechooser.setPreferredSize(new Dimension(longueur,largeur));

                int valider = filechooser.showSaveDialog(frame);
                if(valider == JFileChooser.APPROVE_OPTION) {
                    fichieradjacence = filechooser.getSelectedFile();
                    
                    String extension = fichieradjacence.getName();
                    extension = extension.substring(extension.lastIndexOf('.')+1);
                    extension = extension.replaceAll(String.valueOf('\n'),"");
                    if(!extension.contentEquals("csv")&&!extension.contentEquals("txt")){
                        throw new WrongExtensionException();
                    }
                    if(!fichieradjacence.exists()){
                        fichieradjacence.createNewFile();
                    }
                    valid = true;
                    System.out.println("You save as this file: " + fichieradjacence.getName());
                        JOptionPane.showMessageDialog(filechooser,"File save successfuly","Success",JOptionPane.INFORMATION_MESSAGE);
                }
                else if(valider == JFileChooser.CANCEL_OPTION){
                    throw new CancelSelection();
                }
            }
            catch(WrongExtensionException wee){
                wee.errorFrame(frame);
            }
            catch(IOException IOE){
                
            }
        }
        return fichieradjacence;
    }
    
    private static class InterfaceSelection extends JFrame {
        protected int longueur;
        protected int largeur;
        
        JMenuItem reload,save,saveitem;
        JMenu sommet,arete;
        
        int combobypass1=0,combobypass2=0;
        JPanel ecran2;
        public InterfaceSelection() {
            super("Doc Doc Go");

            longueur = tailleEcran.width * 2 / 3;
            largeur = tailleEcran.height * 2 / 3;
            
            Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
            

            // Menubar
            JMenu filemenu = new JMenu("Fichier");
            JMenu menu=new JMenu("Menu");
            sommet=new JMenu("Sommet");
            arete=new JMenu("Arête");
            JMenu propos=new JMenu("À propos");
            
            JMenuBar menubar = new JMenuBar();
            
            reload =new JMenuItem("Recharger");
            save=new JMenuItem("Enregistrer");
            saveitem = new JMenuItem("Enregistrer sous ");
            JMenuItem openitem = new JMenuItem("Ouvrir fichier");
            JMenuItem quititem = new JMenuItem("Quitter");
            JMenuItem complexite=new JMenuItem("Algorithme de recherche de chemin");
            JMenuItem nbarete=new JMenuItem("Nombre d'arêtes du graphe");
            JMenuItem cararete=new JMenuItem("Caractéristiques d'une arête");
            JMenuItem allaretegraphe=new JMenuItem("Lister toutes les arêtes du graphe");
            JMenuItem listsommettrie=new JMenuItem("Lister tous les sommets du graphe triés par type");
            JMenuItem listsommettype=new JMenuItem("Lister tous les sommets d'un type donné du graphe");
            JMenuItem nbsommet=new JMenuItem("Nombre de sommets dans le graphe");
            JMenuItem nbsommettype=new JMenuItem("Nombre de sommets d'un type donné dans le graphe");
            JMenuItem voisinscommuns=new JMenuItem("Lister les voisins communs de 2 sommets donnés du graphe");
            JMenuItem deuxdistance=new JMenuItem("Savoir si 2 sommets sont 2-distants");
            JMenuItem voisins=new JMenuItem("Lister les voisins directs d'un sommet");
            JMenuItem voisinstype=new JMenuItem("Lister les voisins directs d'un type donné d'un sommet");
            JMenuItem cheminfiable=new JMenuItem("Trouver le chemin le plus fiable entre 2 sommets");
            JMenuItem cheminvitesse=new JMenuItem("Trouver le chemin le plus rapide entre 2 sommets");
            JMenuItem modifiersommet=new JMenuItem("Modifier un sommet");
            JMenuItem modifierarete=new JMenuItem("Modifier une arête");
            JMenuItem comparersommet=new JMenuItem("Comparer 2 sommets sur les plans opératoires,nutritionnels et maternités");
            
            reload.setEnabled(false);
            reload.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        reload(fileadjacence,filesuccesseur);
                        ecran2=(new GraphePanel());
                            content.add(ecran2);
                            layoutmainframe.next(content);
                        
                    } catch (InvalidFileContentException ifce) {
                        ifce.errorFrame(null);
                    }
                }
            });
            
            save.setEnabled(false);
            saveitem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    save(fileadjacence,filesuccesseur);
                }
            });
            
            saveitem.setEnabled(false);
            saveitem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    File[] temp = savefichier(null);
                    if (temp!=null){
                        fileadjacence=temp[0];
                        filesuccesseur=temp[1];
                    }
                }
            });
            
            openitem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        File[] temp = openfichier(null);
                        if (temp != null) {
                            fileadjacence=temp[0];
                            filesuccesseur=temp[1];
                            ecran2=(new GraphePanel());
                            content.add(ecran2);
                            layoutmainframe.next(content);
                            saveitem.setEnabled(true);
                            save.setEnabled(true);
                            reload.setEnabled(true);
                            sommet.setEnabled(true);
                            arete.setEnabled(true);
                            layoutmainframe.last(content);
                        }
                    } catch (InvalidFileContentException ifce) {
                        ifce.errorFrame(null);
                    }
                }
            });
            
            quititem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });
            
            complexite.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JFrame frame=new JFrame();
                    
                    JTextArea texte=new JTextArea();
                    
                    texte.setText("Les algorithmes de recherche de chemin utilisés sont basés sur l'algorithme de Dijkstra. La complexité dans le pire des cas de l'algorithme de recherche du chemin le plus fiable est "+Graphe.getcomplexitecheminfiable()+".");
                    texte.setLineWrap(true);
                    texte.setWrapStyleWord(true);
                    texte.setEditable(false);
                    
                    JScrollPane scrollpane=new JScrollPane(texte);
                    scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                    scrollpane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
                    
                    frame.getContentPane().add(scrollpane);
                    
                    frame.setTitle("À propos des algorithmes de recherche de chemin");
                    frame.setVisible(true);
                    frame.setSize(300,200);
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                }
            });
            
            nbarete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int nb=Arete.getnbarete();
                    String text=nb>0?"Il y a "+nb+" arêtes dans le graphe.":"Il n y a aucune arête dans le graphe.";
                    JOptionPane.showMessageDialog(null,text,"Nombre d'arêtes dans le graphe.",JOptionPane.INFORMATION_MESSAGE);
                }
            });
            
            allaretegraphe.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JTextArea text=new JTextArea();
                    text.setEditable(false);
                    text.setLineWrap(true);
                    text.setWrapStyleWord(true);
                    List<Arete> listarete=graphe.getallarete();
                    for(int i=listarete.size()-1;i>=0;i--){
                        Arete arete=listarete.get(i);
                        text.setText(text.getText()+arete.getExtremite(0)+"-"+arete.getExtremite(1)+"\n");
                    }
                    JScrollPane scrollpane=new JScrollPane(text);
                    scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                    scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
                    JOptionPane optionpane=new JOptionPane(scrollpane,JOptionPane.INFORMATION_MESSAGE);
                    JDialog dialog=optionpane.createDialog("Liste des arêtes du graphe");
                    dialog.setSize(dialog.getWidth(), tailleEcran.height-50);
                    dialog.setVisible(true);
                }
            });
            
            listsommettrie.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JTextArea text=new JTextArea();
                    text.setEditable(false);
                    text.setLineWrap(true);
                    text.setWrapStyleWord(true);
                    List<Sommet> listsommet=graphe.getallsommettrie();
                    for(int i=listsommet.size()-1;i>=0;i--){
                        Sommet sommet=listsommet.get(i);
                        String typetext=null;
                        switch(sommet.gettype()){
                            case 1:
                                typetext="Maternité";
                                break;
                            case 2:
                                typetext="Centre de nutrition";
                                break;
                            case 3:
                                typetext="Bloc opératoire";
                                break;
                        }
                        text.setText(text.getText()+sommet.getnom()+" : "+typetext+"\n");
                    }
                    JScrollPane scrollpane=new JScrollPane(text);
                    scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                    scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
                    JOptionPane optionpane=new JOptionPane(scrollpane,JOptionPane.INFORMATION_MESSAGE);
                    JDialog dialog=optionpane.createDialog("Liste des sommets du graphe");
                    dialog.setMaximumSize(new Dimension(dialog.getWidth(), tailleEcran.height-50));
                    dialog.setVisible(true);
                }
            });
            
            listsommettype.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un type de sommet.");
                    JComboBox<String> combotype=new JComboBox<>(new String[]{"Maternité","Centre de nutrition","Bloc opératoire"});
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            int type=combotype.getSelectedIndex()+1;
                            String typetext=(String)combotype.getSelectedItem();
                            typetext=typetext.toLowerCase();
                            List<Sommet> listsommet=graphe.getallsommettype(type);
                            Collections.reverse(listsommet);
                            JOptionPane.showMessageDialog(null,listsommet,"Sommets de type "+typetext+".",JOptionPane.INFORMATION_MESSAGE);
                            dialog.dispose();
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    combotype.setMaximumSize(new Dimension(120,20));
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combotype);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,150);
                    dialog.setVisible(true);
                }
            });
            
            voisins.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommet=(Sommet) combo.getSelectedItem();
                            List<Sommet> listvoisins=sommet.getvoisins();
                            Collections.reverse(listvoisins);
                            JOptionPane.showMessageDialog(null,listvoisins,"Sommets voisins de "+sommet+".",JOptionPane.INFORMATION_MESSAGE);
                            dialog.dispose();
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,150);
                    dialog.setVisible(true);
                }
            });
            
            voisinstype.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JComboBox<String> combotype=new JComboBox<>(new String[]{"Maternité","Centre de nutrition","Bloc opératoire"});
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommet=(Sommet) combo.getSelectedItem();
                            int type=combotype.getSelectedIndex()+1;
                            String typetext=(String)combotype.getSelectedItem();
                            typetext=typetext.toLowerCase();
                            List<Sommet> listvoisins=sommet.getvoisinstype(type);
                            Collections.reverse(listvoisins);
                            JOptionPane.showMessageDialog(null,listvoisins,"Sommets voisins de "+sommet+" de type "+typetext+".",JOptionPane.INFORMATION_MESSAGE);
                            dialog.dispose();
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    combotype.setMaximumSize(new Dimension(120,20));
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalStrut(5));
                    panelcombo.add(combotype);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,175);
                    dialog.setVisible(true);
                }
            });
            
            cararete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Arete> listarete=graphe.getallarete();
                    Collections.reverse(listarete);
                    Arete[] aretes=listarete.toArray(new Arete[listarete.size()]);
                    JComboBox<Arete> combo=new JComboBox<Arete>(aretes);
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'une arête.");
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Arete arete=(Arete) combo.getSelectedItem();
                            Sommet sommet1=arete.getExtremite(0);
                            Sommet sommet2=arete.getExtremite(1);
                            String text="L'arête relie "+sommet1+" et "+sommet2+".\nFiabilité : "+arete.getfiable()*100+"% \nDistance : "+arete.getdist()+"km \nDurée : "+arete.getduree()+"min";
                            JOptionPane.showMessageDialog(null,text,"Sommets voisins de "+sommet+" de type .",JOptionPane.INFORMATION_MESSAGE);
                            dialog.dispose();
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,150);
                    dialog.setVisible(true);
                }
            });
            
            nbsommet.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int nb=Sommet.getnbsommet();
                    String text=nb>0?"Il y a "+nb+" sommets dans le graphe.":"Il n y a aucun sommet dans le graphe.";
                    JOptionPane.showMessageDialog(null,text,"Nombre de sommets dans le graphe.",JOptionPane.INFORMATION_MESSAGE);
                }
            });
            
            nbsommettype.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un type de sommet.");
                    JComboBox<String> combotype=new JComboBox<>(new String[]{"Maternité","Centre de nutrition","Bloc opératoire"});
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            int type=combotype.getSelectedIndex()+1;
                            String typetext=(String)combotype.getSelectedItem();
                            typetext=typetext.toLowerCase();
                            int nb=graphe.getnbsommettype(type);
                            String text=nb>0?"Il y a "+nb+" sommets de type "+typetext+" dans le graphe.":"Il n y a aucun sommet de type "+typetext+" dans le graphe.";
                            JOptionPane.showMessageDialog(null,text,"Nombre de sommets dans le graphe.",JOptionPane.INFORMATION_MESSAGE);
                            dialog.dispose();
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    combotype.setMaximumSize(new Dimension(120,20));
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combotype);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,150);
                    dialog.setVisible(true);
                }
            });
            
            voisinscommuns.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JComboBox<Sommet> combo2=new JComboBox<>(sommets);
                    combo2.setSelectedIndex(1);
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommet=(Sommet) combo.getSelectedItem();
                            Sommet sommet2=(Sommet) combo2.getSelectedItem();
                            if(sommet!=sommet2){
                                List<Sommet> listvoisins=graphe.getsommetcommuns(sommet, sommet2);
                                Collections.reverse(listvoisins);
                                JOptionPane.showMessageDialog(null,listvoisins,"Sommets voisins communs de "+sommet+" et "+sommet2+".",JOptionPane.INFORMATION_MESSAGE);
                                dialog.dispose(); 
                            }
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    combo.removeItemAt(1);
                    combo.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass1==0) {
                                Sommet sommetselect=(Sommet) combo.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo2.getSelectedItem();
                                combo2.removeAllItems();
                                combobypass2=2;//on fait en sorte de bypass 2 fois l'itemlistener
                                for(Sommet sommet:listtemp){
                                    combo2.addItem(sommet);
                                }
                                combo2.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass1>0){
                                combobypass1--;
                            }
                        }
                    });
                    
                    combo2.setMaximumSize(new Dimension(120,20));
                    combo2.removeItemAt(0);
                    combo2.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass2==0) {
                                Sommet sommetselect=(Sommet) combo2.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo.getSelectedItem();
                                combo.removeAllItems();
                                combobypass1=2;
                                for(Sommet sommet:listtemp){
                                    combo.addItem(sommet);
                                }
                                combo.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass2>0){
                                combobypass2--;
                            }
                        }
                    });
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalStrut(5));
                    panelcombo.add(combo2);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,175);
                    dialog.setVisible(true);
                }
            });
            
            deuxdistance.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JComboBox<Sommet> combo2=new JComboBox<>(sommets);
                    combo2.setSelectedIndex(1);
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommet=(Sommet) combo.getSelectedItem();
                            Sommet sommet2=(Sommet) combo2.getSelectedItem();
                            if(sommet!=sommet2){
                                boolean distance=graphe.sommets2distance(sommet, sommet2);
                                String text=distance==true?" sont 2-distants":" ne sont pas 2-distants.";
                                JOptionPane.showMessageDialog(null,sommet+" et "+sommet2+text,sommet+" et "+sommet2,JOptionPane.INFORMATION_MESSAGE);
                                dialog.dispose(); 
                            }
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    combo.removeItemAt(1);
                    combo.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass1==0) {
                                Sommet sommetselect=(Sommet) combo.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo2.getSelectedItem();
                                combo2.removeAllItems();
                                combobypass2=2;//on fait en sorte de bypass 2 fois l'itemlistener
                                for(Sommet sommet:listtemp){
                                    combo2.addItem(sommet);
                                }
                                combo2.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass1>0){
                                combobypass1--;
                            }
                        }
                    });
                    
                    combo2.setMaximumSize(new Dimension(120,20));
                    combo2.removeItemAt(0);
                    combo2.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass2==0) {
                                Sommet sommetselect=(Sommet) combo2.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo.getSelectedItem();
                                combo.removeAllItems();
                                combobypass1=2;
                                for(Sommet sommet:listtemp){
                                    combo.addItem(sommet);
                                }
                                combo.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass2>0){
                                combobypass2--;
                            }
                        }
                    });
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalStrut(5));
                    panelcombo.add(combo2);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,175);
                    dialog.setVisible(true);
                }
            });
            
            cheminfiable.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JComboBox<Sommet> combo2=new JComboBox<>(sommets);
                    combo2.setSelectedIndex(1);
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommet=(Sommet) combo.getSelectedItem();
                            Sommet sommet2=(Sommet) combo2.getSelectedItem();
                            if(sommet!=sommet2){
                                Chemin chemin=graphe.recherchecheminfiable(sommet, sommet2);
                                String text=chemin.meilleurchemin.getFirst().toString();
                                chemin.meilleurchemin.removeFirst();
                                for(Sommet sommetchemin:chemin.meilleurchemin){
                                    text=sommetchemin+"-"+text;
                                }
                                text=text+"\nFiabilité : "+chemin.fiabilite*100+"% \nDistance : "+chemin.distance+"km \nDurée : "+chemin.duree+"min";
                                JOptionPane.showMessageDialog(null,text,"Chemin le plus fiable entre "+sommet+" et "+sommet2,JOptionPane.INFORMATION_MESSAGE);
                                dialog.dispose(); 
                            }
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    combo.removeItemAt(1);
                    combo.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass1==0) {
                                Sommet sommetselect=(Sommet) combo.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo2.getSelectedItem();
                                combo2.removeAllItems();
                                combobypass2=2;//on fait en sorte de bypass 2 fois l'itemlistener
                                for(Sommet sommet:listtemp){
                                    combo2.addItem(sommet);
                                }
                                combo2.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass1>0){
                                combobypass1--;
                            }
                        }
                    });
                    
                    combo2.setMaximumSize(new Dimension(120,20));
                    combo2.removeItemAt(0);
                    combo2.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass2==0) {
                                Sommet sommetselect=(Sommet) combo2.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo.getSelectedItem();
                                combo.removeAllItems();
                                combobypass1=2;
                                for(Sommet sommet:listtemp){
                                    combo.addItem(sommet);
                                }
                                combo.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass2>0){
                                combobypass2--;
                            }
                        }
                    });
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalStrut(5));
                    panelcombo.add(combo2);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,175);
                    dialog.setVisible(true);
                }
            });
            
            cheminvitesse.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JComboBox<Sommet> combo2=new JComboBox<>(sommets);
                    combo2.setSelectedIndex(1);
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommet=(Sommet) combo.getSelectedItem();
                            Sommet sommet2=(Sommet) combo2.getSelectedItem();
                            if(sommet!=sommet2){
                                Chemin chemin=graphe.recherchecheminrapide(sommet, sommet2);
                                String text=chemin.meilleurchemin.getFirst().toString();
                                chemin.meilleurchemin.removeFirst();
                                for(Sommet sommetchemin:chemin.meilleurchemin){
                                    text=sommetchemin+"-"+text;
                                }
                                text=text+"\nFiabilité : "+chemin.fiabilite*100+"% \nDistance : "+chemin.distance+"km \nDurée : "+chemin.duree+"min \nVitesse moyenne : "+chemin.distance/(chemin.duree/60)+"km/h";
                                JOptionPane.showMessageDialog(null,text,"Chemin le plus rapide entre "+sommet+" et "+sommet2,JOptionPane.INFORMATION_MESSAGE);
                                dialog.dispose(); 
                            }
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    combo.removeItemAt(1);
                    combo.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass1==0) {
                                Sommet sommetselect=(Sommet) combo.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo2.getSelectedItem();
                                combo2.removeAllItems();
                                combobypass2=2;//on fait en sorte de bypass 2 fois l'itemlistener
                                for(Sommet sommet:listtemp){
                                    combo2.addItem(sommet);
                                }
                                combo2.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass1>0){
                                combobypass1--;
                            }
                        }
                    });
                    
                    combo2.setMaximumSize(new Dimension(120,20));
                    combo2.removeItemAt(0);
                    combo2.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass2==0) {
                                Sommet sommetselect=(Sommet) combo2.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo.getSelectedItem();
                                combo.removeAllItems();
                                combobypass1=2;
                                for(Sommet sommet:listtemp){
                                    combo.addItem(sommet);
                                }
                                combo.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass2>0){
                                combobypass2--;
                            }
                        }
                    });
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalStrut(5));
                    panelcombo.add(combo2);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,175);
                    dialog.setVisible(true);
                }
            });
            
            modifiersommet.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommetmodif=(Sommet) combo.getSelectedItem();
                            JDialog dialogmodif=new JDialog((JFrame)null,"Modifier "+sommetmodif);
                            JLabel labelnom=new JLabel("Nom : ");
                            JTextArea fieldnom=new JTextArea(sommetmodif.getnom());
                            fieldnom.setColumns(60);
                            fieldnom.setMaximumSize(new Dimension(60, 20));
                            JPanel panelnom=new JPanel();
                            JComboBox<String> combotype=new JComboBox<String>(new String[]{"Maternité","Centre de nutrition","Bloc opératoire"});
                            combotype.setSelectedIndex(sommetmodif.gettype()-1);
                            combotype.setMaximumSize(new Dimension(120,20));
                            combotype.setAlignmentX(Component.CENTER_ALIGNMENT);
                            JButton confirm=new JButton("Confirmer");
                            confirm.setAlignmentX(Component.CENTER_ALIGNMENT);
                            confirm.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e1) {
                                    String nom=fieldnom.getText();
                                    nom=nom.replaceAll("[!.?;:,§%$£¤^¨*=+@^`#~/]","");
                                    sommetmodif.setnom(nom);
                                    sommetmodif.settype(combotype.getSelectedIndex()+1);
                                    dialogmodif.dispose();
                                    dialog.dispose();
                                }
                            });
                            panelnom.setLayout(new BoxLayout(panelnom,BoxLayout.X_AXIS));
                            panelnom.add(Box.createHorizontalGlue());
                            panelnom.add(labelnom);
                            panelnom.add(Box.createHorizontalStrut(5));
                            panelnom.add(fieldnom);
                            panelnom.add(Box.createHorizontalGlue());
                            
                            dialogmodif.getContentPane().setLayout(new BoxLayout(dialogmodif.getContentPane(),BoxLayout.Y_AXIS));
                            dialogmodif.getContentPane().add(Box.createVerticalGlue());
                            dialogmodif.getContentPane().add(panelnom);
                            dialogmodif.getContentPane().add(Box.createVerticalGlue());
                            dialogmodif.getContentPane().add(combotype);
                            dialogmodif.getContentPane().add(Box.createVerticalGlue());
                            dialogmodif.getContentPane().add(confirm);
                            dialogmodif.getContentPane().add(Box.createVerticalGlue());
                            dialogmodif.setAlwaysOnTop(true);
                            dialogmodif.setSize(300,120);
                            dialogmodif.setResizable(false);
                            dialogmodif.setVisible(true);
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,150);
                    dialog.setVisible(true);
                }
            });
            
            modifierarete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Arete> listarete=graphe.getallarete();
                    Collections.reverse(listarete);
                    Arete[] aretes=listarete.toArray(new Arete[listarete.size()]);
                    JComboBox<Arete> combo=new JComboBox<Arete>(aretes);
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'une arête.");
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Arete aretemodif=(Arete) combo.getSelectedItem();
                            JDialog dialogmodif=new JDialog((JFrame)null,"Modifier "+aretemodif.getExtremite(0)+"-"+aretemodif.getExtremite(1));
                            JButton confirm=new JButton("Confirmer");
                            JLabel labelfiable=new JLabel("Fiabilité : "),labeldistance=new JLabel("Distance : "),labelduree=new JLabel("Durée : ");
                            JTextArea textfiable=new JTextArea(String.valueOf(aretemodif.getfiable()*100));
                            JTextArea textdistance=new JTextArea(String.valueOf(aretemodif.getduree()));
                            JTextArea textduree=new JTextArea(String.valueOf(aretemodif.getduree()));
                            JPanel panelmodif=new JPanel();
                            
                            panelmodif.setLayout(new GridLayout(3,2,0,5));
                            panelmodif.add(labelfiable);
                            panelmodif.add(textfiable);
                            panelmodif.add(labeldistance);
                            panelmodif.add(textdistance);
                            panelmodif.add(labelduree);
                            panelmodif.add(textduree);
                            
                            confirm.setAlignmentX(Component.CENTER_ALIGNMENT);
                            confirm.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e1) {
                                    try{
                                        Double fiabilite=Double.valueOf(textfiable.getText());
                                        Double distance=Double.valueOf(textdistance.getText());
                                        Double duree=Double.valueOf(textduree.getText());
                                        fiabilite=fiabilite/100;
                                        if(fiabilite>0&&fiabilite<1&&duree>0&&distance>0){
                                            aretemodif.setFiable(fiabilite);
                                            aretemodif.setDist(distance);
                                            aretemodif.setDuree(duree);
                                            dialogmodif.dispose();
                                            dialog.dispose();  
                                        }
                                    }
                                    catch(NumberFormatException nfe){
                                        
                                    }
                                }
                            });
                            
                            dialogmodif.add(panelmodif);
                            dialogmodif.getContentPane().add(confirm,BorderLayout.SOUTH);
                            dialogmodif.setAlwaysOnTop(true);
                            dialogmodif.setSize(300,120);
                            dialogmodif.setResizable(false);
                            dialogmodif.setVisible(true);
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,150);
                    dialog.setVisible(true);
                }
            });
            
            comparersommet.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<Sommet> listsommet=graphe.getallsommet();
                    JDialog dialog=new JDialog((Frame)null,"Sélection d'un sommet.");
                    Collections.reverse(listsommet);
                    Sommet[] sommets=listsommet.toArray(new Sommet[listsommet.size()]);
                    JComboBox<Sommet> combo=new JComboBox<>(sommets);
                    JComboBox<Sommet> combo2=new JComboBox<>(sommets);
                    combo2.setSelectedIndex(1);
                    JButton ok=new JButton("Ok");
                    JButton cancel=new JButton("Cancel");
                    JPanel panelbouton=new JPanel();
                    JPanel panelcombo=new JPanel();
                    
                    ok.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e1) {
                            Sommet sommet=(Sommet) combo.getSelectedItem();
                            Sommet sommet2=(Sommet) combo2.getSelectedItem();
                            if(sommet!=sommet2){
                                JDialog dialogcomp=new JDialog((JFrame)null,"Comparer "+sommet+" et "+sommet2);
                                JButton confirm=new JButton("Ok");
                                JPanel panelcomp=new JPanel();
                                JLabel labelsommet1=new JLabel(sommet.toString());
                                labelsommet1.setHorizontalAlignment(JLabel.CENTER);
                                labelsommet1.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel labelsommet2=new JLabel(sommet2.toString());
                                labelsommet2.setHorizontalAlignment(JLabel.CENTER);
                                labelsommet2.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel maternite=new JLabel("Maternité");
                                maternite.setHorizontalAlignment(JLabel.CENTER);
                                maternite.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel nbmaternite1=new JLabel();
                                nbmaternite1.setHorizontalAlignment(JLabel.CENTER);
                                nbmaternite1.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel nbmaternite2=new JLabel();
                                nbmaternite2.setHorizontalAlignment(JLabel.CENTER);
                                nbmaternite2.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel nutritionnel=new JLabel("Centre de nutrition");
                                nutritionnel.setHorizontalAlignment(JLabel.CENTER);
                                nutritionnel.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel nbnutritionnel1=new JLabel();
                                nbnutritionnel1.setHorizontalAlignment(JLabel.CENTER);
                                nbnutritionnel1.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel nbnutritionnel2=new JLabel();
                                nbnutritionnel2.setHorizontalAlignment(JLabel.CENTER);
                                nbnutritionnel2.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel operatoire=new JLabel("Bloc opératoire");
                                operatoire.setHorizontalAlignment(JLabel.CENTER);
                                operatoire.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel nboperatoire1=new JLabel();
                                nboperatoire1.setHorizontalAlignment(JLabel.CENTER);
                                nboperatoire1.setBorder(BorderFactory.createLineBorder(Color.black));
                                
                                JLabel nboperatoire2=new JLabel();
                                nboperatoire2.setHorizontalAlignment(JLabel.CENTER);
                                nboperatoire2.setBorder(BorderFactory.createLineBorder(Color.black));
                            
                                panelcomp.setLayout(new GridLayout(4,3,0,0));
                                
                                ComparaisonSommet comp=graphe.comparersommets(sommet, sommet2, 1);
                                nbmaternite1.setText(String.valueOf(comp.nbtype1));
                                nbmaternite2.setText(String.valueOf(comp.nbtype2));
                                if(comp.nbtype1==comp.nbtype2){
                                    nbmaternite1.setForeground(Color.decode("#f7d600"));
                                    nbmaternite2.setForeground(Color.decode("#f7d600"));
                                }
                                else if(comp.nbtype1<comp.nbtype2){
                                    nbmaternite1.setForeground(Color.decode("#07c400"));
                                    nbmaternite2.setForeground(Color.red);
                                }
                                else{
                                    nbmaternite1.setForeground(Color.red);
                                    nbmaternite2.setForeground(Color.decode("#07c400"));
                                }

                                comp=graphe.comparersommets(sommet, sommet2, 2);
                                nbnutritionnel1.setText(String.valueOf(comp.nbtype1));
                                nbnutritionnel2.setText(String.valueOf(comp.nbtype2));
                                if(comp.nbtype1==comp.nbtype2){
                                    nbnutritionnel1.setForeground(Color.decode("#f7d600"));
                                    nbnutritionnel2.setForeground(Color.decode("#f7d600"));
                                }
                                else if(comp.nbtype1<comp.nbtype2){
                                    nbnutritionnel1.setForeground(Color.decode("#07c400"));
                                    nbnutritionnel2.setForeground(Color.red);
                                }
                                else{
                                    nbnutritionnel1.setForeground(Color.red);
                                    nbnutritionnel2.setForeground(Color.decode("#07c400"));
                                }

                                comp=graphe.comparersommets(sommet, sommet2, 3);
                                nboperatoire1.setText(String.valueOf(comp.nbtype1));
                                nboperatoire2.setText(String.valueOf(comp.nbtype2));
                                if(comp.nbtype1==comp.nbtype2){
                                    nboperatoire1.setForeground(Color.decode("#f7d600"));
                                    nboperatoire2.setForeground(Color.decode("#f7d600"));
                                }
                                else if(comp.nbtype1<comp.nbtype2){
                                    nboperatoire1.setForeground(Color.decode("#07c400"));
                                    nboperatoire2.setForeground(Color.red);
                                }
                                else{
                                    nboperatoire1.setForeground(Color.red);
                                    nboperatoire2.setForeground(Color.decode("#07c400"));
                                }
                                
                                confirm.setAlignmentX(Component.CENTER_ALIGNMENT);
                                confirm.addActionListener(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e1) {
                                        dialogcomp.dispose();
                                        dialog.dispose();
                                    }
                                });

                                panelcomp.add(Box.createHorizontalGlue());
                                panelcomp.add(labelsommet1);
                                panelcomp.add(labelsommet2);
                                panelcomp.add(maternite);
                                panelcomp.add(nbmaternite1);
                                panelcomp.add(nbmaternite2);
                                panelcomp.add(nutritionnel);
                                panelcomp.add(nbnutritionnel1);
                                panelcomp.add(nbnutritionnel2);
                                panelcomp.add(operatoire);
                                panelcomp.add(nboperatoire1);
                                panelcomp.add(nboperatoire2);
                                
                                dialogcomp.add(panelcomp);
                                dialogcomp.getContentPane().add(confirm,BorderLayout.SOUTH);
                                dialogcomp.setAlwaysOnTop(true);
                                dialogcomp.setSize(600,200);
                                dialogcomp.setResizable(false);
                                dialogcomp.setVisible(true);
                                dialog.dispose(); 
                            }
                        }
                    });
                    
                    cancel.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });
                    
                    
                    
                    combo.setMaximumSize(new Dimension(120,20));
                    combo.removeItemAt(1);
                    combo.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass1==0) {
                                Sommet sommetselect=(Sommet) combo.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo2.getSelectedItem();
                                combo2.removeAllItems();
                                combobypass2=2;//on fait en sorte de bypass 2 fois l'itemlistener
                                for(Sommet sommet:listtemp){
                                    combo2.addItem(sommet);
                                }
                                combo2.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass1>0){
                                combobypass1--;
                            }
                        }
                    });
                    
                    combo2.setMaximumSize(new Dimension(120,20));
                    combo2.removeItemAt(0);
                    combo2.addItemListener(new ItemListener() {
                        @Override
                        public void itemStateChanged(ItemEvent e1) {
                            if (e1.getStateChange() == ItemEvent.SELECTED&&combobypass2==0) {
                                Sommet sommetselect=(Sommet) combo2.getSelectedItem();
                                List<Sommet> listtemp=graphe.getallsommet();
                                listtemp.remove(sommetselect);
                                Collections.reverse(listtemp);
                                sommetselect=(Sommet) combo.getSelectedItem();
                                combo.removeAllItems();
                                combobypass1=2;
                                for(Sommet sommet:listtemp){
                                    combo.addItem(sommet);
                                }
                                combo.setSelectedItem(sommetselect);
                            }
                            else if(e1.getStateChange() == ItemEvent.SELECTED&&combobypass2>0){
                                combobypass2--;
                            }
                        }
                    });
                    
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.add(ok);
                    panelbouton.add(Box.createHorizontalStrut(5));
                    panelbouton.add(cancel);
                    panelbouton.add(Box.createHorizontalGlue());
                    panelbouton.setLayout(new BoxLayout(panelbouton,BoxLayout.X_AXIS));
                    
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.add(combo);
                    panelcombo.add(Box.createVerticalStrut(5));
                    panelcombo.add(combo2);
                    panelcombo.add(Box.createVerticalGlue());
                    panelcombo.setLayout(new BoxLayout(panelcombo,BoxLayout.Y_AXIS));
                    
                    dialog.add(panelcombo);
                    dialog.add(panelbouton,BorderLayout.SOUTH);
                    dialog.setSize(250,175);
                    dialog.setVisible(true);
                }
            });
            
            filemenu.add(reload);
            filemenu.add(openitem);
            filemenu.add(save);
            filemenu.add(saveitem);
            filemenu.add(new JSeparator());
            filemenu.add(quititem);
            
            menu.add(sommet);
            menu.add(new JSeparator());
            menu.add(arete);
            
            propos.add(complexite);
            
            sommet.add(listsommettrie);
            sommet.add(listsommettype);
            sommet.add(new JSeparator());
            sommet.add(nbsommet);
            sommet.add(nbsommettype);
            sommet.add(new JSeparator());
            sommet.add(voisins);
            sommet.add(voisinstype);
            sommet.add(voisinscommuns);
            sommet.add(deuxdistance);
            sommet.add(comparersommet);
            sommet.add(new JSeparator());
            sommet.add(cheminfiable);
            sommet.add(cheminvitesse);
            sommet.add(new JSeparator());
            sommet.add(modifiersommet);
            sommet.setEnabled(false);
            
            arete.add(nbarete);
            arete.add(allaretegraphe);
            arete.add(new JSeparator());
            arete.add(cararete);
            arete.add(modifierarete);
            arete.setEnabled(false);
            
            menubar.add(filemenu);
            menubar.add(menu);
            menubar.add(propos);
            //menubar.setBackground(Color.decode("#ccf5ff"));//à approfondir pour changer la couleur du menu en gardant l'effet de relief
            
            this.setJMenuBar(menubar);
            
            PanelEcran1 ecran1 = new PanelEcran1( /*this*/);
            //ecran2 = new JPanel();
            
            content = (JPanel) this.getContentPane();

            // Settings
            this.add(ecran1);
            //this.add(ecran2);
            this.setLayout(layoutmainframe);
            this.setSize(new Dimension(longueur, largeur));
            this.setVisible(true);
            this.setLocationRelativeTo(null);
            this.setResizable(true);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setIconImage( new ImageIcon("logo.png").getImage());
        }

        private class PanelEcran1 extends JPanel{
            public PanelEcran1(/*InterfaceSelection interfaceselection*/){
                super();
                // Interface 2
                setLayout(new BorderLayout());
                setBackground(Color.WHITE);
                
                JPanel paneltitle = new JPanel();
                JPanel panelbtn = new JPanel();
                
                JLabel title = new JLabel();
                title.setAlignmentX(CENTER_ALIGNMENT);
                title.setIcon(new ImageIcon( new ImageIcon("logotext-dark.png").getImage().getScaledInstance(longueur*5/8, largeur*4/9, Image.SCALE_SMOOTH)));
                
                JButton boutonselect = new JButton("Select files");
                
                Border loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
                
                title.setFont(new Font("Serif", Font.BOLD, 30));
                
                boutonselect.addActionListener((ActionEvent e) -> {
                    try {
                        File[] temp = openfichier(null);
                        if (temp != null) {
                            fileadjacence=temp[0];
                            filesuccesseur=temp[1];
                            ecran2=(new GraphePanel());
                            content.add(ecran2);
                            layoutmainframe.next(content);
                            saveitem.setEnabled(true);
                            save.setEnabled(true);
                            reload.setEnabled(true);
                            sommet.setEnabled(true);
                            arete.setEnabled(true);
                        }
                    } catch (InvalidFileContentException ifce) {
                        ifce.errorFrame(null);
                    }
                });
                boutonselect.setForeground(Color.black);
                boutonselect.setBorder(loweredetched);
                boutonselect.setContentAreaFilled(false);
                boutonselect.setFocusPainted(false);
                boutonselect.setBorder(new RoundBtn(20));
                boutonselect.setPreferredSize(new Dimension(130, 100));
                boutonselect.setAlignmentX(CENTER_ALIGNMENT);
                boutonselect.setAlignmentY(CENTER_ALIGNMENT);
                
                paneltitle.add(title);
                paneltitle.setOpaque(false);
                
                panelbtn.setLayout(new BoxLayout(panelbtn, BoxLayout.X_AXIS));
                panelbtn.setOpaque(false);
                panelbtn.add(Box.createHorizontalGlue());
                panelbtn.add(boutonselect);
                panelbtn.add(Box.createHorizontalGlue());
                
                add(paneltitle, BorderLayout.NORTH);
                add(panelbtn, BorderLayout.CENTER);
                }
        }
        
        private class GraphePanel extends JPanel {

            private Random random;
            ArrayList<MaillonAffichageGraphe> listcoord;

            public GraphePanel() {
                this.random = new Random();
                listcoord =new ArrayList<MaillonAffichageGraphe>();
                for(Sommet sommet:graphe.getallsommet()){
                    int x=getRandomNumber(10,longueur-30);
                    int y = getRandomNumber(10, largeur-80);
                    listcoord.add(new MaillonAffichageGraphe(sommet,x,y));
                }
            }

            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;

                for (Arete arete:graphe.getallarete()){
                    MaillonAffichageGraphe extremite1=null,extremite2=null;
                    for(MaillonAffichageGraphe maillon:listcoord){
                        if (maillon.sommet==arete.getExtremite(0)){
                            extremite1=maillon;
                        }
                        else if(maillon.sommet==arete.getExtremite(1)){
                            extremite2=maillon;
                        }
                    }

                    g2d.drawLine(extremite1.x, extremite1.y, extremite2.x, extremite2.y);
                }

                for (MaillonAffichageGraphe maillon : listcoord) {
                    g2d.fillOval(maillon.x-5, maillon.y-5, 10, 10);
                    g2d.drawString(maillon.sommet.getnom(), maillon.x, maillon.y);
                }
                this.repaint();
            }

            private int getRandomNumber(int min, int max) {
                return random.nextInt(max - min) + min;
            }

            private class MaillonAffichageGraphe{
                protected Sommet sommet;
                protected int x;
                protected int y;;

                public MaillonAffichageGraphe(Sommet sommet,int x,int y){
                    this.sommet=sommet;
                    this.x=x;
                    this.y=y;
                }
            }
        }
        
        class RoundBtn implements Border {
            private int r;

            RoundBtn(int r) {
                this.r = r;
            }

            @Override
            public Insets getBorderInsets(Component c) {
                return new Insets(this.r + 1, this.r + 1, this.r + 2, this.r);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }

            @Override
            public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
                g.drawRoundRect(x, y, width - 1, height - 1, r, r);
            }
        }
    }
}


